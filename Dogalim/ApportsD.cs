﻿namespace Dogalim
{
    public class ApportsD
    {
     
        public double Energie { get; set; }

        public double Protéines { get; set; }

        public double MGrasses { get; set; }

        public double Ca { get; set; }

        public double P { get; set; }
    }
}
