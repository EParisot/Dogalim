﻿namespace Dogalim
{
    public class ApportMoy
    {
        public double Am1 { get; set; }

        public double Am2 { get; set; }

        public double Am3 { get; set; }

        public double Am4 { get; set; }

        public double AmTotal { get; set; }
    }
}
