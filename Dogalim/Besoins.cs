﻿using System.Collections.Generic;

namespace Dogalim
{
    public class Besoins : List<double>
    {

        public string Q { get; set; }

        public double eau { get; set; }

        public double E { get; set; }

        public double Prot { get; set; }

        public string MG { get; set; }

        public double Ca { get; set; }

        public double P { get; set; }
    }
}
