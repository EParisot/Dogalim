﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Dogalim
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()                                                                                                              // Définit DataDirectory    
        {
            InitializeComponent();

            AppDomain.CurrentDomain.SetData("DataDirectory",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));     // modifie le chemin d'acces au DataDirectory (+ modif AppConf)
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SplashScreen screen = new SplashScreen(@"Images\splashscreen.bmp");
            screen.Show(true);

            Mouse.OverrideCursor = Cursors.Wait;          

            LoadAlim();

            CBAlim1.SelectedIndex = 0;
            CBAlim2.SelectedIndex = 0;
            CBAlim3.SelectedIndex = 0;
            CBAlim4.SelectedIndex = 0;

            dog.Cat = 1;
            dog.Temp = 1;
            dog.Etat = 1;

            Mouse.OverrideCursor = null;
            screen.Show(false);
        }


        private void SauvegarderBDD_Click(object sender, RoutedEventArgs e)                                                              // Charger une Bdd 
        {
            Microsoft.Win32.SaveFileDialog savedlg = new Microsoft.Win32.SaveFileDialog();
            savedlg.FileName = "MaBddDogalim";
            savedlg.DefaultExt = ".mdf";
            savedlg.Filter = "Mdf documents (.mdf)|*.mdf";

            Nullable<bool> result = savedlg.ShowDialog();

            if (result == true)
            {
                if (File.Exists(savedlg.FileName))
                    File.Delete(savedlg.FileName);

                string directory;
                directory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Dogalim\BDDDogalim.mdf";

                using (BDDDogalimEntities dbEntities = new BDDDogalimEntities())
                {
                    string backupQuery = @"BACKUP DATABASE ""{0}"" TO DISK = N'{1}'";
                    backupQuery = string.Format(backupQuery, directory, savedlg.FileName);
                    dbEntities.Database.SqlQuery<object>(backupQuery).ToList().FirstOrDefault();
                }
            }
        }

        private void ChargerBDD_Click(object sender, RoutedEventArgs e)                                                                  // Charger une Bdd
        {
            if (MessageBox.Show("Voulez vous vraiment Charger une nouvelle Base de Données ? \r\n\r\n(Ceci effacera la Base de Données actuelle)", "Confirmation Chargement Base de Données", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {

                Microsoft.Win32.OpenFileDialog opendlg = new Microsoft.Win32.OpenFileDialog();
                opendlg.Filter = "Mdf documents (.mdf)|*.mdf";
                opendlg.DefaultExt = ".mdf";

                Nullable<bool> result = opendlg.ShowDialog();

                if (result == true)
                {
                    Mouse.OverrideCursor = Cursors.Wait;

                    using (BDDDogalimEntities dbEntities = new BDDDogalimEntities())
                    {
                        string directory;
                        directory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Dogalim\BDDDogalim.mdf";

                        string restoreQuery = @"USE [Master]; 
                                                ALTER DATABASE ""{0}"" SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
                                                RESTORE DATABASE ""{0}"" FROM DISK='{1}' WITH REPLACE;
                                                ALTER DATABASE ""{0}"" SET MULTI_USER;";
                        restoreQuery = string.Format(restoreQuery, directory, opendlg.FileName);
                        var list = dbEntities.Database.SqlQuery<object>(restoreQuery).ToList();
                        var Result = list.FirstOrDefault();

                    }

                    LoadAlim();
                    Mouse.OverrideCursor = null;
                }
            }
        }

        private void SauvegarderRation_Click(object sender, RoutedEventArgs e)                                                           // Sauvegarder une ration 
        {           

            dog.cat = listBoxCategorie.SelectedIndex;
            dog.temp = listBoxTemp.SelectedIndex;
            dog.etat = listBoxEtat.SelectedIndex;

            dog.Reg = cbRegime.SelectedIndex;

            dog.Alim1 = CBAlim1.SelectedIndex;
            dog.Alim2 = CBAlim2.SelectedIndex;
            dog.Alim3 = CBAlim3.SelectedIndex;
            dog.Alim4 = CBAlim4.SelectedIndex;

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Ration " + tbNom.Text;
            dlg.DefaultExt = ".xml";
            dlg.Filter = "Xml documents (.xml)|*.xml";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                dog.Enregistrer(dlg.FileName);

                MessageBox.Show("Ration enregistrée avec succès. \r\n\r\nSi vous utilisez des aliments personnalisés, pensez à également enregistrer votre Base de Données.", "Enregistrer ma ration");
            }
        }

        private void ChargerRation_Click(object sender, RoutedEventArgs e)                                                               // Charger une ration 
        {
            if (MessageBox.Show("Voulez vous vraiment Charger une nouvelle ration ? \r\n\r\n(Ceci effacera la ration actuelle) \r\n\r\nSi vous utilisez des aliments personnalisés, pensez à importer la Base de Données adaptée.", "Confirmation Chargement Ration", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {

                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.FileName = "MaRation";
                dlg.DefaultExt = ".xml";
                dlg.Filter = "Xml documents (.xml)|*.xml";

                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                  
                    Dog dog = Dog.Charger(dlg.FileName);

                    tbNom.Text = dog.Name;
                    lbSexe.SelectedIndex = dog.Sex;
                    tbAge.Text = dog.Age.ToString();
                    tbCom.Text = dog.Com;

                    tbPV.Text = dog.PV.ToString();
                    listBoxCategorie.SelectedIndex = dog.cat;
                    listBoxTemp.SelectedIndex = dog.temp;
                    listBoxEtat.SelectedIndex = dog.etat;

                    cbRegime.SelectedIndex = dog.Reg;

                    CBAlim1.SelectedIndex = dog.Alim1;
                    CBAlim2.SelectedIndex = dog.Alim2;
                    CBAlim3.SelectedIndex = dog.Alim3;
                    CBAlim4.SelectedIndex = dog.Alim4;

                    tbprecision.Text = (10*dog.precision).ToString();



                    Mouse.OverrideCursor = null;


                }
            }
        }


        private void Imprimer_Click(object sender, RoutedEventArgs e)                                                                    // Imprimer page
        {
            PrintDialog printDialog = new PrintDialog();

            bool? dialogResult = printDialog.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
                if (TabControl.SelectedIndex == 0)
                    printDialog.PrintVisual(RationSP, "Dogalim : Ration " + tbCom);

                if (TabControl.SelectedIndex == 1)
                    printDialog.PrintVisual(PrixSP, "Dogalim : Prix de la Ration " + tbCom);

                if (TabControl.SelectedIndex == 3)
                    printDialog.PrintVisual(AlimSP, "Dogalim : Aliments");
            }
        }

        private void Aide_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Bienvenue sur Dogalim. \r\n\r\nDogalim est un calculateur dynamique de rations alimentaires pour chiens. \r\n\r\nCe logiciel va vous donner la possibilité de calculer, évaluer, modifier à volonté une ration alimentaire pour votre chien selon ses propres caractéristiques (poids, tempérament, etc...). \r\n\r\n- Pour commencer, veuillez renseigner les données de base de votre chien (nom, âge, etc...) et indiquez son \"Poids Vif\" (poids mesuré en condition normale), et éventuellement une \"Catégorie\", un \"Tempérament\" et un \"Etat\". \r\n\r\nLes besoins spécifiques de votre chien commencent à apparaitre dans la ligne \"Besoins\". \r\n\r\n- Ensuite, indiquez selon quel régime vous nourrissez votre chien (ration ménagère ou industrielle). \r\n\r\n- Pour finir, choisissez vos aliments. \r\n\r\nLes quantités sont automatiquement calculées afin de couvrir au mieux les besoins énergétiques de votre chien. \r\n\r\nVous pouvez également ajouter/modifier des aliments à la base de données (mais pas en supprimer). \r\n\r\nAfin de déterminer à partir de quel seuil les cellules se colorent en rouge, en vert ou en bleu, veuillez modifier la \"Précision\". \r\n\r\n- Une fois vos modifications terminées, vous pouvez enregistrer votre ration depuis le menu \"Rations\", \"Enregistrer ma Ration\". \r\n\r\nATTENTION : Si vous avez modifié et/ou ajouté des aliments, veillez à également exporter la base de données correspondante et pensez toujours à utiliser la même en l'ouvrant, afin de disposer de la bonne liste d'aliments. \r\n\r\n- Vous pouvez exporter et importer vos bases de données depuis le menu \"Base de données\". \r\n\r\nIl vous est toujours possible de restorer une base de données d'origine en téléchargant la base disponible sur dogalim.free.fr puis en l'important depuis le logiciel. ", "Aide Dogalim : ");
        }

        private void Infos_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Dogalim Version Bêta 0.1 \r\n\r\ndogalim.free.fr \r\n\r\nConcept / développement : Eric PARISOT", "Infos Dogalim : ");
        }


        private void btninfosphy_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Renseignement optionnel", "Infos Etat Physiologique");
        }

        private void btninfostemp_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Renseignement optionnel", "Infos Tempérament");
        }

        private void btninfoscat_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Renseignement optionnel", "Infos Catégorie");
        }


        Dog dog = new Dog();


        private void tbNom_TextChanged(object sender, TextChangedEventArgs e)                                                            // Evennement Nom changé
        {
            string name = tbNom.Text;

            dog.Name = name;
        }

        private void tbSexe_SelectionChanged(object sender, SelectionChangedEventArgs e)                                                 // Evennement Sexe changé
        {
            dog.Sex = lbSexe.SelectedIndex;
        }

        private void tbAge_TextChanged(object sender, TextChangedEventArgs e)                                                            // Evennement Age changé
        {
            int age;
            int.TryParse(tbAge.Text, out age);

            dog.Age = age;
        }

        private void tbComm_TextChanged(object sender, TextChangedEventArgs e)                                                           // Evennement Comm changé
        {
            string com = tbCom.Text;

            dog.Com = com;
        }



        private void tbPV_TextChanged(object sender, TextChangedEventArgs e)                                                              // Evennement PoidsVif changé
        {
            int pv;
            int.TryParse(tbPV.Text, out pv);

            dog.PV = pv;

            CalcBesoins();

            CalcApportsP();

            CalcSommeDiff();

            PrixRation();
        }

        private void listBoxCategorie_SelectionChanged(object sender, SelectionChangedEventArgs e)                                        // Evennement Categorie changé
        {

            double cat;

            switch (listBoxCategorie.SelectedIndex)

            {
                default:
                    cat = 1;
                    break;
                case 0:
                    cat = 0.8;
                    break;
                case 1:
                    cat = 0.9;
                    break;
                case 2:
                    cat = 1.1;
                    break;
                case 3:
                    cat = 1;
                    break;
            }

            dog.Cat = cat;

            CalcBesoins();

            CalcApportsP();

            CalcSommeDiff();

            PrixRation();
        }

        private void listBoxTemp_SelectionChanged(object sender, SelectionChangedEventArgs e)                                             // Evennement Temp changé
        {
            double temp;

            switch (listBoxTemp.SelectedIndex)

            {
                default:
                    temp = 1;
                    break;
                case 0:
                    temp = 0.7;
                    break;
                case 1:
                    temp = 0.8;
                    break;
                case 2:
                    temp = 0.9;
                    break;
                case 3:
                    temp = 1;
                    break;
                case 4:
                    temp = 1.1;
                    break;
                case 5:
                    temp = 1.2;
                    break;
            }

            dog.Temp = temp;

            CalcBesoins();

            CalcApportsP();

            CalcSommeDiff();

            PrixRation();
        }

        private void listBoxEtat_SelectionChanged(object sender, SelectionChangedEventArgs e)                                             // Evennement Etat changé
        {
            double etat;

            switch (listBoxEtat.SelectedIndex)

            {
                default:
                    etat = 1;
                    break;
                case 0:
                    etat = 3;
                    break;
                case 1:
                    etat = 2;
                    break;
                case 2:
                    etat = 1.5;
                    break;
                case 3:
                    etat = 1;
                    break;
                case 4:
                    etat = 1.1;
                    break;
                case 5:
                    etat = 1.5;
                    break;
                case 6:
                    etat = 3;
                    break;
                case 7:
                    etat = 1.75;
                    break;
                case 8:
                    etat = 1.85;
                    break;
            }

            dog.Etat = etat;

            CalcBesoins();

            CalcApportsP();

            CalcSommeDiff();

            PrixRation();
        }



        Memoire memoire = new Memoire();                                                                                                  // Memoirise et replace les aliments dans Comboboxes selon Régime

        private void cbRegime_SelectionChanged(object sender, SelectionChangedEventArgs e)                                                // Evennement Regime changé
        {
          

            switch (cbRegime.SelectedIndex)
            {
                default:

                    Label1.Text = "Vide";
                    Label2.Text = "Vide";
                    Label3.Text = "Vide";
                    Label4.Text = "Vide";

                    memoire.mem = false;


                    break;
                case 0:


                    if (memoire.mem == true)
                    {
                        CBAlim1.SelectedIndex = memoire.mem1;
                        CBAlim2.SelectedIndex = memoire.mem2;
                        CBAlim3.SelectedIndex = memoire.mem3;
                        CBAlim4.SelectedIndex = memoire.mem4;
                    }

                    memoire.mem = false;


                    Label1.Text = "Viande :";
                    Label2.Text = "Féculents :";
                    Label3.Text = "Légumes :";
                    Label4.Text = "Divers :";

                    TblRation.RowDefinitions[2].Height = new GridLength(1, GridUnitType.Star);
                    TblRation.RowDefinitions[3].Height = new GridLength(1, GridUnitType.Star);
                    TblRation.RowDefinitions[4].Height = new GridLength(1, GridUnitType.Star);



                    break;
                case 1:

                    if (memoire.mem == true)
                    {
                        CBAlim1.SelectedIndex = memoire.mem1;
                        CBAlim2.SelectedIndex = memoire.mem2;
                        CBAlim3.SelectedIndex = memoire.mem3;
                        CBAlim4.SelectedIndex = memoire.mem4;
                    }

                    memoire.mem = false;


                    Label1.Text = "Viande :";
                    Label2.Text = "Féculents :";
                    Label3.Text = "Légumes :";
                    Label4.Text = "Divers :";

                    TblRation.RowDefinitions[2].Height = new GridLength(1, GridUnitType.Star);
                    TblRation.RowDefinitions[3].Height = new GridLength(1, GridUnitType.Star);
                    TblRation.RowDefinitions[4].Height = new GridLength(1, GridUnitType.Star);



                    break;
                case 2:


                    memoire.mem1 = CBAlim1.SelectedIndex;
                    memoire.mem2 = CBAlim2.SelectedIndex;
                    memoire.mem3 = CBAlim3.SelectedIndex;
                    memoire.mem4 = CBAlim4.SelectedIndex;
                    memoire.mem = true;


                    Label1.Text = "Alim Indus:";
                    Label2.Text = "Vide";
                    Label3.Text = "Vide";
                    Label4.Text = "Vide";

                    CBAlim1.SelectedIndex = 0;
                    TBAlim1.Text = string.Empty;

                    TblRation.RowDefinitions[2].Height = new GridLength(0);                  
                    CBAlim2.SelectedIndex = 0;
                    TBAlim2.Text = string.Empty;

                    TblRation.RowDefinitions[3].Height = new GridLength(0);
                    CBAlim3.SelectedIndex = 0;
                    TBAlim3.Text = string.Empty;

                    TblRation.RowDefinitions[4].Height = new GridLength(0);
                    CBAlim4.SelectedIndex = 0;
                    TBAlim4.Text = string.Empty;

                    break;             
            }

            LoadAlim();

            CalcBesoins();

            CalcApportsP();

            CalcSommeDiff();

            PrixRation();


        }


        private void LoadAlim ()                                                                                                          // Charge les listes d'aliments dans les ComboBoxes
        {

            if (cbRegime.SelectedIndex == -1 || cbRegime.SelectedIndex == 0 || cbRegime.SelectedIndex == 1)
            {

                using (var context = new BDDDogalimEntities())
                {
                   
                    var query1 = from c in context.Viandes
                                 where c.Id > 0
                                 select new { c.ImagePath, c.Aliment };
                    var results1 = query1.ToList();
                    CBAlim1.ItemsSource = results1;
                    cbalim1.ItemsSource = results1;


                    var query2 = from c in context.Feculents
                                 where c.Id > 0
                                 select new { c.ImagePath, c.Aliment };
                    var results2 = query2.ToList();
                    CBAlim2.ItemsSource = results2;
                    cbalim2.ItemsSource = results2;

                    var query3 = from c in context.Legumes
                                 where c.Id > 0
                                 select new { c.ImagePath, c.Aliment };
                    var results3 = query3.ToList();
                    CBAlim3.ItemsSource = results3;
                    cbalim3.ItemsSource = results3;

                    var query4 = from c in context.Divers
                                 where c.Id > 0
                                 select new { c.ImagePath, c.Aliment };
                    var results4 = query4.ToList();
                    CBAlim4.ItemsSource = results4;
                    cbalim4.ItemsSource = results4;

                }
            }

            if (cbRegime.SelectedIndex == 2)
            {
                using (var context = new BDDDogalimEntities())
                {
                    var query1 = from c in context.AlimIndus
                                 where c.Id > 0
                                 select new { c.ImagePath, c.Aliment };
                    var results1 = query1.ToList();
                    CBAlim1.ItemsSource = results1;
                    cbalim1.ItemsSource = results1;

                }
            }

            using (var context = new BDDDogalimEntities())
            {
                var query1 = from c in context.Viandes
                             where c.Id > 0
                             select new { c.ImagePath, c.Aliment, c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                var results1 = query1.ToList();
                viandesDataGrid.ItemsSource = results1;

                var query2 = from c in context.Feculents
                             where c.Id > 0
                             select new { c.ImagePath, c.Aliment, c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                var results2 = query2.ToList();
                feculentsDataGrid.ItemsSource = results2;

                var query3 = from c in context.Legumes
                             where c.Id > 0
                             select new { c.ImagePath, c.Aliment, c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                var results3 = query3.ToList();
                legumesDataGrid.ItemsSource = results3;

                var query4 = from c in context.Divers
                             where c.Id > 0
                             select new { c.ImagePath, c.Aliment, c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                var results4 = query4.ToList();
                diversDataGrid.ItemsSource = results4;

                var query5 = from c in context.AlimIndus
                             where c.Id > 0
                             select new { c.ImagePath, c.Aliment, c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                var results5 = query5.ToList();
                alimIndusDataGrid.ItemsSource = results5;

            }

            using (var context = new BDDDogalimEntities())
            {

                var query1 = from c in context.Viandes
                             where c.Id > 0
                             select new { c.ImagePath, c.Aliment };
                var results1 = query1.ToList();

                var query2 = from c in context.Feculents
                             where c.Id > 1
                             select new { c.ImagePath, c.Aliment };
                var results2 = query2.ToList();

                var query3 = from c in context.Legumes
                             where c.Id > 1
                             select new { c.ImagePath, c.Aliment };
                var results3 = query3.ToList();

                var query4 = from c in context.Divers
                             where c.Id > 1
                             select new { c.ImagePath, c.Aliment };
                var results4 = query4.ToList();

                var query5 = from c in context.AlimIndus
                             where c.Id > 1
                             select new { c.ImagePath, c.Aliment };
                var results5 = query5.ToList();


                results1.AddRange(results2);
                results1.AddRange(results3);
                results1.AddRange(results4);
                results1.AddRange(results5);

                cbalimtoedit.ItemsSource = results1;
            }

        }


        ApportMoy apportmoy = new ApportMoy();

        private void CalcApportMoy()                                                                                                       // Calcul Apport Moyen de la ration
        {

            using (var context = new BDDDogalimEntities())
            {
                if (cbRegime.SelectedIndex < 2)
                {
                    var query1 = from c in context.Viandes
                                 where c.Id == CBAlim1.SelectedIndex + 1
                                 select c.Energie;
                    var result1 = query1.ToList();
                    foreach (var i in result1)
                    {
                        apportmoy.Am1 = (double)i;
                    }
                }               
                if ( cbRegime.SelectedIndex == 2)
                {
                    var query1 = from c in context.AlimIndus
                                 where c.Id == CBAlim1.SelectedIndex + 1
                                 select c.Energie;
                    var result1 = query1.ToList();
                    foreach (var i in result1)
                    {
                        apportmoy.Am1 = (double)i;
                    }
                }


                var query2 = from c in context.Feculents
                             where c.Id == CBAlim2.SelectedIndex + 1
                             select c.Energie;
                var result2 = query2.ToList();
                foreach (var i in result2)
                {
                    apportmoy.Am2 = (double)i;
                }

                var query3 = from c in context.Legumes
                             where c.Id == CBAlim3.SelectedIndex + 1
                             select c.Energie;
                var result3 = query3.ToList();
                foreach (var i in result3)
                {
                    apportmoy.Am3 = (double)i;
                }

                var query4 = from c in context.Divers
                             where c.Id == CBAlim4.SelectedIndex + 1
                             select c.Energie;
                var result4 = query4.ToList();
                foreach (var i in result4)
                {
                    apportmoy.Am4 = (double)i;
                }
            }
            switch (cbRegime.SelectedIndex)
            {
                case -1:

                    TBEmoy.Text = "Régime?";


                    break;

                case 0:

                    apportmoy.AmTotal = (4 * apportmoy.Am1 + 3 * apportmoy.Am2 + 2 * apportmoy.Am3 + apportmoy.Am4) / 10;

                    TBEmoy.Text = apportmoy.AmTotal.ToString();

                    break;

                case 1:

                    apportmoy.AmTotal = (3 * apportmoy.Am1 + 3 * apportmoy.Am2 + 3 * apportmoy.Am3 + apportmoy.Am4) / 10;

                    TBEmoy.Text = apportmoy.AmTotal.ToString();

                    break;

                case 2:

                    apportmoy.AmTotal = apportmoy.Am1 ;

                    TBEmoy.Text = apportmoy.AmTotal.ToString();

                    break;

            }

            if (TBEmoy.Text == "0")
                TBEmoy.Text = "Aliments?";

        }


        Besoins besoins = new Besoins();
        List<Besoins> ListBesoins = new List<Besoins>();

        public void CalcBesoins()                                                                                                          // Calcul Besoins
        {
            CalcApportMoy();

            besoins.eau = (60 * dog.PV);

            besoins.E = Math.Round(Math.Pow(dog.PV, 0.75) * 132 * dog.Cat * dog.Temp * dog.Etat);

            if (cbRegime.SelectedIndex != -1)
            {
                if (apportmoy.AmTotal != 0)
                {
                    double q = Math.Round(1000 * (besoins.E / apportmoy.AmTotal));
                    besoins.Q = q.ToString();

                    switch (cbRegime.SelectedIndex)
                    {
                        case 0:

                            apportP1.Q = 0.4 * q;

                            TBAlim1.Text = (0.4 * q).ToString();


                            apportP2.Q = 0.3 * q;

                            TBAlim2.Text = (0.3 * q).ToString();


                            apportP3.Q = 0.2 * q;

                            TBAlim3.Text = (0.2 * q).ToString();


                            apportP4.Q = 0.1 * q;

                            TBAlim4.Text = (0.1 * q).ToString();

                            break;

                        case 1:

                            apportP1.Q = 0.3 * q;

                            TBAlim1.Text = (0.3 * q).ToString();


                            apportP2.Q = 0.3 * q;

                            TBAlim2.Text = (0.3 * q).ToString();


                            apportP3.Q = 0.3 * q;

                            TBAlim3.Text = (0.3 * q).ToString();


                            apportP4.Q = 0.1 * q;

                            TBAlim4.Text = (0.1 * q).ToString();

                            break;

                        case 2:

                       
                            apportP1.Q = q;
                          
                            TBAlim1.Text = besoins.Q;                           
                           
                            

                            break;
                    }
                }
                else
                    besoins.Q = "Aliments?";
            }
            else
                besoins.Q = "Régime?";

            if (listBoxEtat.SelectedIndex == 0 || listBoxEtat.SelectedIndex == 1 || listBoxEtat.SelectedIndex == 2)
                besoins.Prot = 9.6 * dog.PV;
            else
                besoins.Prot = 4.8 * dog.PV;

            if (besoins.Q != "Régime?")
            {
                if (besoins.Q != "Aliments?")
                {
                    double Q;
                    double.TryParse(besoins.Q, out Q);
                    besoins.MG = (Q * 0.1).ToString();
                }
                else
                    besoins.MG = "Aliments?";
            }
            else
                besoins.MG = "Régime?";

            besoins.Ca = 0.25 * dog.PV;

            besoins.P = 0.2 * dog.PV;

            ListBesoins.Clear();
            ListBesoins.Add(besoins);

            DGBesoins.ItemsSource = null;
            DGBesoins.ItemsSource = ListBesoins;
        }


        private void CalcApportsP()                                                                                                        // Calcul ApportsP
        {
            apportP1.MS = Math.Round((double)apport1.MS * apportP1.Q / 1000,2);
            apportP1.E = Math.Round((double)apport1.E * apportP1.Q / 1000, 2);
            apportP1.Prot = Math.Round((double)apport1.Prot * apportP1.Q / 1000, 2);
            apportP1.MG = Math.Round((double)apport1.MG * apportP1.Q / 1000, 2);
            apportP1.Ca = Math.Round((double)apport1.Ca * apportP1.Q / 1000, 2);
            apportP1.P = Math.Round((double)apport1.P * apportP1.Q / 1000, 2);

            ListapportP1.Clear();
            ListapportP1.Add(apportP1);

            DGpAlim1.ItemsSource = null;
            DGpAlim1.ItemsSource = ListapportP1;



            apportP2.MS = Math.Round((double)apport2.MS * apportP2.Q / 1000, 2);
            apportP2.E = Math.Round((double)apport2.E * apportP2.Q / 1000, 2);
            apportP2.Prot = Math.Round((double)apport2.Prot * apportP2.Q / 1000, 2);
            apportP2.MG = Math.Round((double)apport2.MG * apportP2.Q / 1000, 2);
            apportP2.Ca = Math.Round((double)apport2.Ca * apportP2.Q / 1000, 2);
            apportP2.P = Math.Round((double)apport2.P * apportP2.Q / 1000, 2);

            ListapportP2.Clear();
            ListapportP2.Add(apportP2);

            DGpAlim2.ItemsSource = null;
            DGpAlim2.ItemsSource = ListapportP2;



            apportP3.MS = Math.Round((double)apport3.MS * apportP3.Q / 1000, 2);
            apportP3.E = Math.Round((double)apport3.E * apportP3.Q / 1000, 2);
            apportP3.Prot = Math.Round((double)apport3.Prot * apportP3.Q / 1000, 2);
            apportP3.MG = Math.Round((double)apport3.MG * apportP3.Q / 1000, 2);
            apportP3.Ca = Math.Round((double)apport3.Ca * apportP3.Q / 1000, 2);
            apportP3.P = Math.Round((double)apport3.P * apportP3.Q / 1000, 2);

            ListapportP3.Clear();
            ListapportP3.Add(apportP3);

            DGpAlim3.ItemsSource = null;
            DGpAlim3.ItemsSource = ListapportP3;



            apportP4.MS = Math.Round((double)apport4.MS * apportP4.Q / 1000, 2);
            apportP4.E = Math.Round((double)apport4.E * apportP4.Q / 1000, 2);
            apportP4.Prot = Math.Round((double)apport4.Prot * apportP4.Q / 1000, 2);
            apportP4.MG = Math.Round((double)apport4.MG * apportP4.Q / 1000, 2);
            apportP4.Ca = Math.Round((double)apport4.Ca * apportP4.Q / 1000, 2);
            apportP4.P = Math.Round((double)apport4.P * apportP4.Q / 1000, 2);

            ListapportP4.Clear();
            ListapportP4.Add(apportP4);

            DGpAlim4.ItemsSource = null;
            DGpAlim4.ItemsSource = ListapportP4;

           
        }


        ApportsP apportSomme = new ApportsP();
        List<ApportsP> listapportsPSomme = new List<ApportsP>();

        ApportsD apportDiff = new ApportsD();
        List<ApportsD> listapportsPDiff = new List<ApportsD>();

        private void CalcSommeDiff()                                                                                                       // Calcul Somme et Diff
        {
            apportSomme.MS = apportP1.MS + apportP2.MS + apportP3.MS + apportP4.MS;
            apportSomme.E = apportP1.E + apportP2.E + apportP3.E + apportP4.E;
            apportSomme.Prot = apportP1.Prot + apportP2.Prot + apportP3.Prot + apportP4.Prot;
            apportSomme.MG = apportP1.MG + apportP2.MG + apportP3.MG + apportP4.MG;
            apportSomme.Ca = apportP1.Ca + apportP2.Ca + apportP3.Ca + apportP4.Ca;
            apportSomme.P = apportP1.P + apportP2.P + apportP3.P + apportP4.P;

            listapportsPSomme.Clear();
            listapportsPSomme.Add(apportSomme);

            DGpSomme.ItemsSource = null;
            DGpSomme.ItemsSource = listapportsPSomme;

            double Rpe = Math.Round(apportSomme.Prot / apportSomme.E, 2);
            TBRpe.Text = Rpe.ToString();
            if (Rpe < 0.05)
                TBRpe.Background = Brushes.CornflowerBlue;
            if (Rpe >= 0.05 && Rpe <= 0.08)
                TBRpe.Background = Brushes.YellowGreen;
            if (Rpe > 0.08)
                TBRpe.Background = Brushes.IndianRed;

            double Rcap = Math.Round(apportSomme.Ca / apportSomme.P, 2);
            TBRcap.Text = Rcap.ToString();
            if (Rcap < 1.2)
                TBRcap.Background = Brushes.CornflowerBlue;
            if (Rcap >= 1.2 && Rpe <= 2)
                TBRcap.Background = Brushes.YellowGreen;
            if (Rcap > 2)
                TBRcap.Background = Brushes.IndianRed;

            apportDiff.Energie = Math.Round(apportSomme.E - besoins.E, 2);
            apportDiff.Protéines = Math.Round(apportSomme.Prot - besoins.Prot, 2);

            double mgNum;
            double.TryParse(besoins.MG, out mgNum);
            apportDiff.MGrasses = Math.Round(apportSomme.MG - mgNum, 2);

            apportDiff.Ca = Math.Round(apportSomme.Ca - besoins.Ca, 2);
            apportDiff.P = Math.Round(apportSomme.P - besoins.P, 2);

            listapportsPDiff.Clear();
            listapportsPDiff.Add(apportDiff);

            DGpDiff.ItemsSource = null;
            DGpDiff.ItemsSource = listapportsPDiff;

        }


        private void infoprecision_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Renseignez un pourcentage afin de choisir les seuils min et max de coloration des cellules.\r\n\r\n Seuil = Besoin x % précision", "Infos Précision");
        }


        private void tbprecision_TextChanged(object sender, TextChangedEventArgs e)                                                         // Evenement Precision modifiée
        {
            string fact = tbprecision.Text;
            double fa;
            double.TryParse(fact, out fa);
            dog.precision = fa / 100;           

            if (CBAlim1.SelectedIndex != -1)
                CalcSommeDiff();

        }

        private void DGpDiff_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)                                   // Coloration Cellules datagridDiff
        {

            if (e.PropertyName == "Energie")
            {
                if (apportDiff.Energie < -(dog.precision * besoins.E))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                }
                if (apportDiff.Energie >= -(dog.precision * besoins.E) && apportDiff.Energie <= (dog.precision * besoins.E))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                }
                if (apportDiff.Energie > (dog.precision * besoins.E))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                }
            }

            if (e.PropertyName == "Protéines")
            {
                if (apportDiff.Protéines < -(dog.precision * besoins.Prot))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                }
                if (apportDiff.Protéines >= -(dog.precision * besoins.Prot) && apportDiff.Protéines <= (dog.precision * besoins.Prot))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                }
                if (apportDiff.Protéines > (dog.precision * besoins.Prot))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                }
            }

            if (e.PropertyName == "MGrasses")
            {
                double MG;
                double.TryParse(besoins.MG, out MG);

                if (apportDiff.MGrasses < -(dog.precision * MG))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                }
                if (apportDiff.MGrasses >= -(dog.precision * MG) && apportDiff.MGrasses <= (dog.precision * MG))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                }
                if (apportDiff.MGrasses > (dog.precision * MG))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                }

            }

            if (e.PropertyName == "Ca")
            {
                if (apportDiff.Ca < -(dog.precision * besoins.Ca))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                }
                if (apportDiff.Ca >= -(dog.precision * besoins.Ca) && apportDiff.Ca <= (dog.precision * besoins.Ca))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                }
                if (apportDiff.Ca > (dog.precision * besoins.Ca))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                }

            }
            if (e.PropertyName == "P")
            {
                if (apportDiff.P < -(dog.precision * besoins.P))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
                }
                if (apportDiff.P >= -(dog.precision * besoins.P) && apportDiff.P <= (dog.precision * besoins.P))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.YellowGreen)));
                }
                if (apportDiff.P > (dog.precision * besoins.P))
                {
                    e.Column.CellStyle = new Style(typeof(DataGridCell));
                    e.Column.CellStyle.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new SolidColorBrush(Colors.IndianRed)));
                }
            }
        }


        Apports apport1 = new Apports();
        ApportsP apportP1 = new ApportsP();
        List<ApportsP> ListapportP1 = new List<ApportsP>();

        private void CBAlim1_SelectionChanged(object sender, SelectionChangedEventArgs e)                                                   // Evenement Alim1 modifié
        {         

            if (cbRegime.SelectedIndex == -1 || cbRegime.SelectedIndex == 0 || cbRegime.SelectedIndex == 1)
            {
                using (var context = new BDDDogalimEntities())
                {
                    var query = from c in context.Viandes
                                where c.Id == CBAlim1.SelectedIndex + 1
                                select new { c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                    var results = query.ToList();

                    DGAlim1.ItemsSource = results;
                   
                    foreach (var i in results)
                    {
                        apport1.MS = (decimal)i.MSeches;
                        apport1.E = (decimal)i.Energie;
                        apport1.Prot = (decimal)i.Proteines;
                        apport1.MG = (decimal)i.MGrasses;
                        apport1.Ca = (decimal)i.Ca;
                        apport1.P = (decimal)i.P;
                        apport1.Prix = (decimal)i.PrixKg;
                    }              
                }
            }
         
            if (cbRegime.SelectedIndex == 2)
            {
                using (var context = new BDDDogalimEntities())
                {
                    var query = from c in context.AlimIndus
                                where c.Id == CBAlim1.SelectedIndex + 1
                                select new { c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                    var results = query.ToList();

                    DGAlim1.ItemsSource = results;

                    foreach (var i in results)
                    {
                        apport1.MS = (decimal)i.MSeches;
                        apport1.E = (decimal)i.Energie;
                        apport1.Prot = (decimal)i.Proteines;
                        apport1.MG = (decimal)i.MGrasses;
                        apport1.Ca = (decimal)i.Ca;
                        apport1.P = (decimal)i.P;
                        apport1.Prix = (decimal)i.PrixKg;
                    }                    
                }            
            }

            CalcBesoins();

            CalcApportsP();

            CalcSommeDiff();

            PrixRation();

        }

        Apports apport2 = new Apports();
        ApportsP apportP2 = new ApportsP();
        List<ApportsP> ListapportP2 = new List<ApportsP>();

        private void CBAlim2_SelectionChanged(object sender, SelectionChangedEventArgs e)                                                   // Evenement Alim2 modifié
        {

            using (var context = new BDDDogalimEntities())
            {
                var query = from c in context.Feculents
                            where c.Id == CBAlim2.SelectedIndex + 1
                            select new { c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                var results = query.ToList();

                DGAlim2.ItemsSource = results;

                foreach (var i in results)
                {
                    apport2.MS = (decimal)i.MSeches;
                    apport2.E = (decimal)i.Energie;
                    apport2.Prot = (decimal)i.Proteines;
                    apport2.MG = (decimal)i.MGrasses;
                    apport2.Ca = (decimal)i.Ca;
                    apport2.P = (decimal)i.P;
                    apport2.Prix = (decimal)i.PrixKg;
                }
            }  

            CalcBesoins();

            CalcApportsP();

            CalcSommeDiff();

            PrixRation();
        }

        Apports apport3 = new Apports();
        ApportsP apportP3 = new ApportsP();
        List<ApportsP> ListapportP3 = new List<ApportsP>();

        private void CBAlim3_SelectionChanged(object sender, SelectionChangedEventArgs e)                                                   // Evenement Alim3 modifié
        {

            using (var context = new BDDDogalimEntities())
            {
                var query = from c in context.Legumes
                            where c.Id == CBAlim3.SelectedIndex + 1
                            select new { c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                var results = query.ToList();

                DGAlim3.ItemsSource = results;

                foreach (var i in results)
                {
                    apport3.MS = (decimal)i.MSeches;
                    apport3.E = (decimal)i.Energie;
                    apport3.Prot = (decimal)i.Proteines;
                    apport3.MG = (decimal)i.MGrasses;
                    apport3.Ca = (decimal)i.Ca;
                    apport3.P = (decimal)i.P;
                    apport3.Prix = (decimal)i.PrixKg;
                }
            }     

            CalcBesoins();

            CalcApportsP();

            CalcSommeDiff();

            PrixRation();
        }

        Apports apport4 = new Apports();
        ApportsP apportP4 = new ApportsP();
        List<ApportsP> ListapportP4 = new List<ApportsP>();

        private void CBAlim4_SelectionChanged(object sender, SelectionChangedEventArgs e)                                                   // Evenement Alim4 modifié
        {

            using (var context = new BDDDogalimEntities())
            {
                var query = from c in context.Divers
                            where c.Id == CBAlim4.SelectedIndex + 1
                            select new { c.MSeches, c.Energie, c.Proteines, c.MGrasses, c.Ca, c.P, c.PrixKg };
                var results = query.ToList();

                DGAlim4.ItemsSource = results;

                foreach (var i in results)
                {
                    apport4.MS = (decimal)i.MSeches;
                    apport4.E = (decimal)i.Energie;
                    apport4.Prot = (decimal)i.Proteines;
                    apport4.MG = (decimal)i.MGrasses;
                    apport4.Ca = (decimal)i.Ca;
                    apport4.P = (decimal)i.P;
                    apport4.Prix = (decimal)i.PrixKg;
                }
            }
        
            CalcBesoins();

            CalcApportsP();

            CalcSommeDiff();

            PrixRation();
        }



        private void PrixRation()                                                                                                           // Calcul prix ration
        {

            if (CBAlim1.SelectedIndex > 0)
            {
                label1.Content = "Viandes / Alim Indus :";

                if (cbRegime.SelectedIndex == 0 || cbRegime.SelectedIndex == 1)
                {
                    using (BDDDogalimEntities context = new BDDDogalimEntities())
                    {
                        var query = context.Viandes.Where(c => c.Id == CBAlim1.SelectedIndex + 1).First();

                        cbalim1.Visibility = Visibility.Visible;
                        cbalim1.SelectedIndex = CBAlim1.SelectedIndex;
                        apport1.Prix = query.PrixKg;
                        tbprixKg1.Text = apport1.Prix.ToString() + " Euros/Kg";
                        tbQ1.Text = "x " + apportP1.Q.ToString() + " g";
                        tbprix1.Text = "= " + Math.Round((double)apport1.Prix * apportP1.Q/1000,2).ToString() + " Euros/Jour";
                    }
                }
                if (cbRegime.SelectedIndex == 2 )
                {
                    using (BDDDogalimEntities context = new BDDDogalimEntities())
                    {
                        var query = context.AlimIndus.Where(c => c.Id == CBAlim1.SelectedIndex + 1).First();

                        cbalim1.Visibility = Visibility.Visible;
                        cbalim1.SelectedIndex = CBAlim1.SelectedIndex;
                        apport1.Prix = query.PrixKg;
                        tbprixKg1.Text = apport1.Prix.ToString() + " Euros/Kg";
                        tbQ1.Text = "x " + apportP1.Q.ToString() + " g";
                        tbprix1.Text = "= " + Math.Round((double)apport1.Prix * apportP1.Q/1000,2).ToString() + " Euros/Jour";
                    }
                }
            }
            else
            {
                label1.Content = string.Empty;
                cbalim1.Visibility = Visibility.Hidden;
                tbprixKg1.Text = string.Empty;
                tbQ1.Text = string.Empty;
                tbprix1.Text = string.Empty;
            }

            if (CBAlim2.SelectedIndex > 0)
            {
                label2.Content = "Féculents :";

                using (BDDDogalimEntities context = new BDDDogalimEntities())
                {
                    var query = context.Feculents.Where(c => c.Id == CBAlim2.SelectedIndex + 1).First();


                    cbalim2.Visibility = Visibility.Visible;
                    cbalim2.SelectedIndex = CBAlim2.SelectedIndex;
                    apport2.Prix = query.PrixKg;
                    tbprixKg2.Text = apport2.Prix.ToString() + " Euros/Kg";
                    tbQ2.Text = "x " + apportP2.Q.ToString() + " g";
                    tbprix2.Text = "= " + Math.Round((double)apport2.Prix * apportP2.Q/1000,2).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label2.Content = string.Empty;
                cbalim2.Visibility = Visibility.Hidden;
                tbprixKg2.Text = string.Empty;
                tbQ2.Text = string.Empty;
                tbprix2.Text = string.Empty;
            }

            if (CBAlim3.SelectedIndex > 0)
            {
                label3.Content = "Légumes :";

                using (BDDDogalimEntities context = new BDDDogalimEntities())
                {
                    var query = context.Legumes.Where(c => c.Id == CBAlim3.SelectedIndex + 1).First();


                    cbalim3.Visibility = Visibility.Visible;
                    cbalim3.SelectedIndex = CBAlim3.SelectedIndex;
                    apport3.Prix = query.PrixKg;
                    tbprixKg3.Text = apport3.Prix.ToString() + " Euros/Kg";
                    tbQ3.Text = "x " + apportP3.Q.ToString() + " g";
                    tbprix3.Text = "= " + Math.Round((double)apport3.Prix * apportP3.Q/1000,2).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label3.Content = string.Empty;
                cbalim3.Visibility = Visibility.Hidden;
                tbprixKg3.Text = string.Empty;
                tbQ3.Text = string.Empty;
                tbprix3.Text = string.Empty;
            }

            if (CBAlim4.SelectedIndex > 0)
            {
                label4.Content = "Divers :";

                using (BDDDogalimEntities context = new BDDDogalimEntities())
                {
                    var query = context.Divers.Where(c => c.Id == CBAlim4.SelectedIndex + 1).First();


                    cbalim4.Visibility = Visibility.Visible;
                    cbalim4.SelectedIndex = CBAlim4.SelectedIndex;
                    apport4.Prix = query.PrixKg;
                    tbprixKg4.Text = apport4.Prix.ToString() + " Euros/Kg";
                    tbQ4.Text = "x " + apportP4.Q.ToString() + " g";
                    tbprix4.Text = "= " + Math.Round((double)apport4.Prix * apportP4.Q/1000,2).ToString() + " Euros/Jour";
                }
            }
            else
            {
                label4.Content = string.Empty;
                cbalim4.Visibility = Visibility.Hidden;
                tbprixKg4.Text = string.Empty;
                tbQ4.Text = string.Empty;
                tbprix4.Text = string.Empty;
            }

            if (apport1.Prix != null && apport2.Prix != null && apport3.Prix != null && apport4.Prix != null)
            {
                tbQTotal.Text = Math.Round(apportP1.Q + apportP2.Q + apportP3.Q + apportP4.Q,2).ToString() + " g";

                tbprixTotal.Text = Math.Round((((double)apport1.Prix * apportP1.Q/1000) + ((double)apport2.Prix * apportP2.Q/1000) + ((double)apport3.Prix * apportP3.Q/1000) + ((double)apport4.Prix * apportP4.Q/1000)),2).ToString() + " Euros/jour";

                tbprixTotalAn.Text = Math.Round(365 * (((double)apport1.Prix * apportP1.Q/1000) + ((double)apport2.Prix * apportP2.Q/1000) + ((double)apport3.Prix * apportP3.Q/1000) + ((double)apport4.Prix * apportP4.Q/1000)),2).ToString() + " Euros/an";
            }
        }



        private void infocalcalim_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Renseignez les informations nutritives telles qu'indiquées sur le paquet d'aliment industriel,\r\n\r\n(une seule valeur peux rester vide et sera automatiquement calculée)\r\n\r\nLes données seront automatiquement converties au format utilisé dans Dogalim après avoir cliqué sur \"Calculer/convertir\".\r\n\r\nIl vous suffira alors de cliquer sur \"Ajouter un aliment\" pour l'ajouter à la base de données après avoir renseigné sa catégorie et son nom, éventuellement une icône. ", "Infos Calculer l'apport de mon Alim Indus");
        }
        private void btncalcalim_Click(object sender, RoutedEventArgs e)                                                                    // Calcule les valeurs nutritives depuis données paquet
        {
            List<double> apportIndus = new List<double>();

            double gluc;
            double.TryParse(tbGlucides.Text, out gluc);
            apportIndus.Add(gluc);

            double prot;
            double.TryParse(tbProtéines.Text, out prot);
            apportIndus.Add(prot);

            double lip;
            double.TryParse(tbLipides.Text, out lip);
            apportIndus.Add(lip);

            double hum;
            double.TryParse(tbHumidité.Text, out hum);
            apportIndus.Add(hum);

            double cend;
            double.TryParse(tbCendres.Text, out cend);
            apportIndus.Add(cend);

            double cell;
            double.TryParse(tbCellulose.Text, out cell);
            apportIndus.Add(cell);

            double ca;
            double.TryParse(tbCalcium.Text, out ca);
            apportIndus.Add(ca);

            double p;
            double.TryParse(tbPhosphore.Text, out p);
            apportIndus.Add(p);


            double somme = Math.Round(gluc + prot + lip + hum + cend + cell + ca + p,2);
            tbTotal.Text = somme.ToString();


            if (somme > 100)
            {
                tbTotal.Background = Brushes.IndianRed;
                MessageBox.Show("Attention, la somme des éléments dépasse 100%... \r\n\r\nVeuillez vérifier votre saisie.", "Attention");
            }
            if (somme < 100)
            {             
                tbTotal.Background = Brushes.CornflowerBlue;
                            
                var nbzero = apportIndus.FindAll(x => x == 0);

                if (nbzero.Count > 1)
                {
                    MessageBox.Show("Attention, plus d'une valeur est nulle... \r\n\r\nVeuillez compléter votre saisie (une seule valeur nulle autorisée).", "Attention");
                }

                if (nbzero.Count == 1)
                {
                    int missingindex = apportIndus.FindIndex(x => x == 0);

                    if (missingindex == 0)
                    {
                        gluc = 100 - somme;
                        tbGlucides.Text = gluc.ToString();
                    }

                    if (missingindex == 1)
                    {
                        prot = 100 - somme;
                        tbProtéines.Text = prot.ToString();
                    }

                    if (missingindex == 2)
                    {
                        lip = 100 - somme;
                        tbLipides.Text = lip.ToString();
                    }

                    if (missingindex == 3)
                    {
                        hum = 100 - somme;
                        tbHumidité.Text = hum.ToString();
                    }

                    if (missingindex == 4)
                    {
                        cend = 100 - somme;
                        tbCendres.Text = cend.ToString();
                    }

                    if (missingindex == 5)
                    {
                        cell = 100 - somme;
                        tbCellulose.Text = cell.ToString();
                    }

                    if (missingindex == 6)
                    {
                        ca = 100 - somme;
                        tbCalcium.Text = ca.ToString();
                    }

                    if (missingindex == 7)
                    {
                        p = 100 - somme;
                        tbPhosphore.Text = p.ToString();
                    }

                    somme = Math.Round(gluc + prot + lip + hum + cend + cell + ca + p,2);
                    tbTotal.Text = somme.ToString();                                    
                }

                if (somme == 100)
                {
                    tbTotal.Background = Brushes.YellowGreen;

                    tbMStoAdd.Text = (100 - hum).ToString();

                    tbEtoAdd.Text = (10 * ((8.65 * lip) + (3.52 * gluc) + (3.52 * prot))).ToString();

                    tbProttoAdd.Text = (10 * prot).ToString();

                    tbMGtoAdd.Text = (10 * lip).ToString();

                    tbcatoAdd.Text = (10 * ca).ToString();

                    tbptoAdd.Text = (10 * p).ToString();
                }
                else
                    MessageBox.Show("Attention, la somme des éléments n'ateint pas 100%... \r\n\r\nVeuillez vérifier votre saisie.", "Attention");
            }

          
            if (somme == 100)
            {
                tbTotal.Background = Brushes.YellowGreen;

                tbMStoAdd.Text = (100 - hum).ToString();

                tbEtoAdd.Text = (10 * ((8.65 * lip) + (3.52 * gluc) + (3.52 * prot))).ToString();

                tbProttoAdd.Text = (10 * prot).ToString();

                tbMGtoAdd.Text = (10 * lip).ToString();

                tbcatoAdd.Text = (10 * ca).ToString();

                tbptoAdd.Text = (10 * p).ToString();
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Taux d'humidité selon le type d'aliment industriel : \r\n\r\nAliment Sec : 8 - 12 % \r\n\r\nAliment Semi-humide : 20 - 25 % \r\n\r\nAliment Humide : 70 - 80 %", "Infos Humidité");
        }

        private void infoajoutalim_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Si vous connaissez déjà les valeurs nutritives de vos aliments, renseignez ces informations dans les cases appropriées \r\n\r\nIl vous suffit ensuite de cliquer sur \"Ajouter un aliment\" pour l'ajouter à la base de données après avoir renseigné sa catégorie et son nom, éventuellement une icône. ", "Infos Ajouter un Aliment");
        }
        private void btnajoutalim_Click(object sender, RoutedEventArgs e)                                                                   // Ajoute un aliment
        {
            if (tbalimtoAdd.Text != string.Empty && cbCat.SelectedIndex != -1)
            {
                if (MessageBox.Show("Voulez vous vraiment ajouter cet Aliment ? \r\n\r\n(Procédure irréversible)", "Confirmation Ajout", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    using (var context = new BDDDogalimEntities())
                    {
                        if (cbCat.SelectedIndex == 0)
                        {
                            var newalim = context.Viandes.Create();

                            newalim.Id = viandesDataGrid.Items.Count + 1;

                            if (cbIcones.SelectedValue != null)
                            {
                                newalim.ImagePath = (cbIcones.SelectedItem as ComboBoxItem).Tag.ToString();
                            }

                            newalim.Aliment = tbalimtoAdd.Text;

                            decimal msNum;
                            decimal.TryParse(tbMStoAdd.Text, out msNum);
                            newalim.MSeches = msNum;

                            decimal eNum;
                            decimal.TryParse(tbEtoAdd.Text, out eNum);
                            newalim.Energie = eNum;

                            decimal protNum;
                            decimal.TryParse(tbProttoAdd.Text, out protNum);
                            newalim.Proteines = protNum;

                            decimal mgNum;
                            decimal.TryParse(tbMGtoAdd.Text, out mgNum);
                            newalim.MGrasses = mgNum;

                            decimal caNum;
                            decimal.TryParse(tbcatoAdd.Text, out caNum);
                            newalim.Ca = caNum;

                            decimal pNum;
                            decimal.TryParse(tbptoAdd.Text, out pNum);
                            newalim.P = pNum;

                            decimal prixNum;
                            decimal.TryParse(tbprixtoAdd.Text, out prixNum);
                            newalim.PrixKg = prixNum;

                            if (msNum == 0 || eNum == 0 || protNum == 0 || caNum == 0 || pNum == 0 || prixNum == 0)
                            {
                                MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                            }

                            context.Viandes.Add(newalim);

                        }

                        if (cbCat.SelectedIndex == 1)
                        {

                            var newalim = context.Feculents.Create();

                            newalim.Id = feculentsDataGrid.Items.Count + 1;

                            if (cbIcones.SelectedValue != null)
                            {
                                newalim.ImagePath = (cbIcones.SelectedItem as ComboBoxItem).Tag.ToString();
                            }

                            newalim.Aliment = tbalimtoAdd.Text;

                            decimal msNum;
                            decimal.TryParse(tbMStoAdd.Text, out msNum);
                            newalim.MSeches = msNum;

                            decimal eNum;
                            decimal.TryParse(tbEtoAdd.Text, out eNum);
                            newalim.Energie = eNum;

                            decimal protNum;
                            decimal.TryParse(tbProttoAdd.Text, out protNum);
                            newalim.Proteines = protNum;

                            decimal mgNum;
                            decimal.TryParse(tbMGtoAdd.Text, out mgNum);
                            newalim.MGrasses = mgNum;

                            decimal caNum;
                            decimal.TryParse(tbcatoAdd.Text, out caNum);
                            newalim.Ca = caNum;

                            decimal pNum;
                            decimal.TryParse(tbptoAdd.Text, out pNum);
                            newalim.P = pNum;

                            decimal prixNum;
                            decimal.TryParse(tbprixtoAdd.Text, out prixNum);
                            newalim.PrixKg = prixNum;

                            if (msNum == 0 || eNum == 0 || protNum == 0 || caNum == 0 || pNum == 0 || prixNum == 0)
                            {
                                MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                            }

                            context.Feculents.Add(newalim);

                        }

                        if (cbCat.SelectedIndex == 2)
                        {

                            var newalim = context.Legumes.Create();

                            newalim.Id = legumesDataGrid.Items.Count + 1;

                            if (cbIcones.SelectedValue != null)
                            {
                                newalim.ImagePath = (cbIcones.SelectedItem as ComboBoxItem).Tag.ToString();
                            }

                            newalim.Aliment = tbalimtoAdd.Text;

                            decimal msNum;
                            decimal.TryParse(tbMStoAdd.Text, out msNum);
                            newalim.MSeches = msNum;

                            decimal eNum;
                            decimal.TryParse(tbEtoAdd.Text, out eNum);
                            newalim.Energie = eNum;

                            decimal protNum;
                            decimal.TryParse(tbProttoAdd.Text, out protNum);
                            newalim.Proteines = protNum;

                            decimal mgNum;
                            decimal.TryParse(tbMGtoAdd.Text, out mgNum);
                            newalim.MGrasses = mgNum;

                            decimal caNum;
                            decimal.TryParse(tbcatoAdd.Text, out caNum);
                            newalim.Ca = caNum;

                            decimal pNum;
                            decimal.TryParse(tbptoAdd.Text, out pNum);
                            newalim.P = pNum;

                            decimal prixNum;
                            decimal.TryParse(tbprixtoAdd.Text, out prixNum);
                            newalim.PrixKg = prixNum;

                            if (msNum == 0 || eNum == 0 || protNum == 0 || caNum == 0 || pNum == 0 || prixNum == 0)
                            {
                                MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                            }

                            context.Legumes.Add(newalim);

                        }

                        if (cbCat.SelectedIndex == 3)
                        {

                            var newalim = context.Divers.Create();

                            newalim.Id = diversDataGrid.Items.Count + 1;

                            if (cbIcones.SelectedValue != null)
                            {
                                newalim.ImagePath = (cbIcones.SelectedItem as ComboBoxItem).Tag.ToString();
                            }

                            newalim.Aliment = tbalimtoAdd.Text;

                            decimal msNum;
                            decimal.TryParse(tbMStoAdd.Text, out msNum);
                            newalim.MSeches = msNum;

                            decimal eNum;
                            decimal.TryParse(tbEtoAdd.Text, out eNum);
                            newalim.Energie = eNum;

                            decimal protNum;
                            decimal.TryParse(tbProttoAdd.Text, out protNum);
                            newalim.Proteines = protNum;

                            decimal mgNum;
                            decimal.TryParse(tbMGtoAdd.Text, out mgNum);
                            newalim.MGrasses = mgNum;

                            decimal caNum;
                            decimal.TryParse(tbcatoAdd.Text, out caNum);
                            newalim.Ca = caNum;

                            decimal pNum;
                            decimal.TryParse(tbptoAdd.Text, out pNum);
                            newalim.P = pNum;

                            decimal prixNum;
                            decimal.TryParse(tbprixtoAdd.Text, out prixNum);
                            newalim.PrixKg = prixNum;

                            if (msNum == 0 || eNum == 0 || protNum == 0 || caNum == 0 || pNum == 0 || prixNum == 0)
                            {
                                MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                            }

                            context.Divers.Add(newalim);

                        }

                        if (cbCat.SelectedIndex == 4)
                        {

                            var newalim = context.AlimIndus.Create();

                            newalim.Id = alimIndusDataGrid.Items.Count + 1;

                            if (cbIcones.SelectedValue != null)
                            {
                                newalim.ImagePath = (cbIcones.SelectedItem as ComboBoxItem).Tag.ToString();
                            }

                            newalim.Aliment = tbalimtoAdd.Text;

                            decimal msNum;
                            decimal.TryParse(tbMStoAdd.Text, out msNum);
                            newalim.MSeches = msNum;

                            decimal eNum;
                            decimal.TryParse(tbEtoAdd.Text, out eNum);
                            newalim.Energie = eNum;

                            decimal protNum;
                            decimal.TryParse(tbProttoAdd.Text, out protNum);
                            newalim.Proteines = protNum;

                            decimal mgNum;
                            decimal.TryParse(tbMGtoAdd.Text, out mgNum);
                            newalim.MGrasses = mgNum;

                            decimal caNum;
                            decimal.TryParse(tbcatoAdd.Text, out caNum);
                            newalim.Ca = caNum;

                            decimal pNum;
                            decimal.TryParse(tbptoAdd.Text, out pNum);
                            newalim.P = pNum;

                            decimal prixNum;
                            decimal.TryParse(tbprixtoAdd.Text, out prixNum);
                            newalim.PrixKg = prixNum;

                            if (msNum == 0 || eNum == 0 || protNum == 0 || caNum == 0 || pNum == 0 || prixNum == 0)
                            {
                                MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                            }

                            context.AlimIndus.Add(newalim);

                        }

                        context.SaveChanges();

                        tbGlucides.Text = string.Empty;
                        tbProtéines.Text = string.Empty;
                        tbLipides.Text = string.Empty;
                        tbHumidité.Text = string.Empty;
                        tbCendres.Text = string.Empty;
                        tbCellulose.Text = string.Empty;
                        tbCalcium.Text = string.Empty;
                        tbPhosphore.Text = string.Empty;
                        tbTotal.Text = string.Empty;

                        cbIcones.SelectedItem = null;
                        tbalimtoAdd.Text = string.Empty;
                        tbMStoAdd.Text = string.Empty;
                        tbEtoAdd.Text = string.Empty;
                        tbProttoAdd.Text = string.Empty;
                        tbMGtoAdd.Text = string.Empty;
                        tbcatoAdd.Text = string.Empty;
                        tbptoAdd.Text = string.Empty;
                        tbprixtoAdd.Text = string.Empty;

                        LoadAlim();
                    }
                }
            }
            else
            {
                MessageBox.Show("Vous n'avez pas spécifié de Catégorie et/ou de nom pour votre aliment. \r\n\r\nVeuillez sélectionner une catégorie et donner un titre à votre aliment.", "Attention");
            }
        }


        private void infomodifalim_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult lexique = MessageBox.Show("Si vous souhaitez modifier les valeurs nutritives / prix d'un aliment déjà présent dans la base de données, sélectionnez cet aliment dans la liste et modifiez les informations désirées. \r\n\r\nIl vous suffit ensuite de cliquer sur \"Modifier un aliment\" pour enregistrer vos modifications dans la base de données. ", "Infos Modifier un Aliment");
        }
        private void cbalimtoedit_SelectionChanged(object sender, SelectionChangedEventArgs e)                                              // Sélectionne l'aliment à modifier
        {
            if (cbalimtoedit.SelectedItem != null)
            {

                using (var context = new BDDDogalimEntities())
                {
                    if (cbalimtoedit.SelectedIndex + 1 <= viandesDataGrid.Items.Count)
                    {
                        var alimtoedit = context.Viandes.Where(c => c.Id == cbalimtoedit.SelectedIndex + 1).FirstOrDefault();

                        tbMStoedit.Text = alimtoedit.MSeches.ToString();

                        tbEtoedit.Text = alimtoedit.Energie.ToString();

                        tbProttoedit.Text = alimtoedit.Proteines.ToString();

                        tbMGtoedit.Text = alimtoedit.MGrasses.ToString();

                        tbcatoedit.Text = alimtoedit.Ca.ToString();

                        tbptoedit.Text = alimtoedit.P.ToString();

                        tbprixtoedit.Text = alimtoedit.PrixKg.ToString();
                    }

                    if (cbalimtoedit.SelectedIndex + 1 > viandesDataGrid.Items.Count && cbalimtoedit.SelectedIndex + 1 <= viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count)
                    {
                        var alimtoedit = context.Feculents.Where(c => c.Id == cbalimtoedit.SelectedIndex + 2 - viandesDataGrid.Items.Count).FirstOrDefault();

                        tbMStoedit.Text = alimtoedit.MSeches.ToString();

                        tbEtoedit.Text = alimtoedit.Energie.ToString();

                        tbProttoedit.Text = alimtoedit.Proteines.ToString();

                        tbMGtoedit.Text = alimtoedit.MGrasses.ToString();

                        tbcatoedit.Text = alimtoedit.Ca.ToString();

                        tbptoedit.Text = alimtoedit.P.ToString();

                        tbprixtoedit.Text = alimtoedit.PrixKg.ToString();
                    }

                    if (cbalimtoedit.SelectedIndex + 1 > viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count && cbalimtoedit.SelectedIndex + 1 <= viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count)
                    {
                        var alimtoedit = context.Legumes.Where(c => c.Id == cbalimtoedit.SelectedIndex + 3 - (viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count)).FirstOrDefault();

                        tbMStoedit.Text = alimtoedit.MSeches.ToString();

                        tbEtoedit.Text = alimtoedit.Energie.ToString();

                        tbProttoedit.Text = alimtoedit.Proteines.ToString();

                        tbMGtoedit.Text = alimtoedit.MGrasses.ToString();

                        tbcatoedit.Text = alimtoedit.Ca.ToString();

                        tbptoedit.Text = alimtoedit.P.ToString();

                        tbprixtoedit.Text = alimtoedit.PrixKg.ToString();
                    }

                    if (cbalimtoedit.SelectedIndex + 1 > viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count && cbalimtoedit.SelectedIndex + 1 <= viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count + diversDataGrid.Items.Count)
                    {
                        var alimtoedit = context.Divers.Where(c => c.Id == cbalimtoedit.SelectedIndex + 4 - (viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count)).FirstOrDefault();

                        tbMStoedit.Text = alimtoedit.MSeches.ToString();

                        tbEtoedit.Text = alimtoedit.Energie.ToString();

                        tbProttoedit.Text = alimtoedit.Proteines.ToString();

                        tbMGtoedit.Text = alimtoedit.MGrasses.ToString();

                        tbcatoedit.Text = alimtoedit.Ca.ToString();

                        tbptoedit.Text = alimtoedit.P.ToString();

                        tbprixtoedit.Text = alimtoedit.PrixKg.ToString();
                    }

                    if (cbalimtoedit.SelectedIndex + 1 > viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count + diversDataGrid.Items.Count)
                    {
                        var alimtoedit = context.AlimIndus.Where(c => c.Id == cbalimtoedit.SelectedIndex + 5 - (viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count + diversDataGrid.Items.Count)).FirstOrDefault();

                        tbMStoedit.Text = alimtoedit.MSeches.ToString();

                        tbEtoedit.Text = alimtoedit.Energie.ToString();

                        tbProttoedit.Text = alimtoedit.Proteines.ToString();

                        tbMGtoedit.Text = alimtoedit.MGrasses.ToString();

                        tbcatoedit.Text = alimtoedit.Ca.ToString();

                        tbptoedit.Text = alimtoedit.P.ToString();

                        tbprixtoedit.Text = alimtoedit.PrixKg.ToString();
                    }
                }
            }
        }
        private void btnmodifalim_Click(object sender, RoutedEventArgs e)                                                                   // Modifie les valeurs d'un aliment
        {
            if (MessageBox.Show("Voulez vous vraiment modifier cet Aliment ?", "Confirmation Modification", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                if (cbalimtoedit.SelectedIndex + 1 <= viandesDataGrid.Items.Count)
                {
                    Viandes alimnew = new Viandes();

                    string MStoedit = tbMStoedit.Text;
                    decimal MStoeditNum;
                    decimal.TryParse(MStoedit, out MStoeditNum);
                    alimnew.MSeches = MStoeditNum;

                    string Etoedit = tbEtoedit.Text;
                    decimal EtoeditNum;
                    decimal.TryParse(Etoedit, out EtoeditNum);
                    alimnew.Energie = EtoeditNum;

                    string Prottoedit = tbProttoedit.Text;
                    decimal ProttoeditNum;
                    decimal.TryParse(Prottoedit, out ProttoeditNum);
                    alimnew.Proteines = ProttoeditNum;

                    string MGtoedit = tbMGtoedit.Text;
                    decimal MGtoeditNum;
                    decimal.TryParse(MGtoedit, out MGtoeditNum);
                    alimnew.MGrasses = MGtoeditNum;

                    string Catoedit = tbcatoedit.Text;
                    decimal CatoeditNum;
                    decimal.TryParse(Catoedit, out CatoeditNum);
                    alimnew.Ca = CatoeditNum;

                    string Ptoedit = tbptoedit.Text;
                    decimal PtoeditNum;
                    decimal.TryParse(Ptoedit, out PtoeditNum);
                    alimnew.P = PtoeditNum;

                    string Prixtoedit = tbprixtoedit.Text;
                    decimal PrixtoeditNum;
                    decimal.TryParse(Prixtoedit, out PrixtoeditNum);
                    alimnew.PrixKg = PrixtoeditNum;


                    if (MStoeditNum == 0 || EtoeditNum == 0 || ProttoeditNum == 0 || MGtoeditNum == 0 || CatoeditNum == 0 || PtoeditNum == 0 || PrixtoeditNum == 0)
                    {
                        MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                    }

                    using (var context = new BDDDogalimEntities())
                    {

                        var alimtoedit = context.Viandes.Where(c => c.Id == cbalimtoedit.SelectedIndex + 1).First();

                        alimtoedit.MSeches = alimnew.MSeches;

                        alimtoedit.Energie = alimnew.Energie;

                        alimtoedit.Proteines = alimnew.Proteines;

                        alimtoedit.MGrasses = alimnew.MGrasses;

                        alimtoedit.Ca = alimnew.Ca;

                        alimtoedit.P = alimnew.P;

                        alimtoedit.PrixKg = alimnew.PrixKg;


                        context.SaveChanges();



                        LoadAlim();


                        cbalimtoedit.SelectedItem = null;

                    }
                }

                if (cbalimtoedit.SelectedIndex + 1 > viandesDataGrid.Items.Count && cbalimtoedit.SelectedIndex + 1 <= viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count)
                {
                    Feculents alimnew = new Feculents();

                    string MStoedit = tbMStoedit.Text;
                    decimal MStoeditNum;
                    decimal.TryParse(MStoedit, out MStoeditNum);
                    alimnew.MSeches = MStoeditNum;

                    string Etoedit = tbEtoedit.Text;
                    decimal EtoeditNum;
                    decimal.TryParse(Etoedit, out EtoeditNum);
                    alimnew.Energie = EtoeditNum;

                    string Prottoedit = tbProttoedit.Text;
                    decimal ProttoeditNum;
                    decimal.TryParse(Prottoedit, out ProttoeditNum);
                    alimnew.Proteines = ProttoeditNum;

                    string MGtoedit = tbMGtoedit.Text;
                    decimal MGtoeditNum;
                    decimal.TryParse(MGtoedit, out MGtoeditNum);
                    alimnew.MGrasses = MGtoeditNum;

                    string Catoedit = tbcatoedit.Text;
                    decimal CatoeditNum;
                    decimal.TryParse(Catoedit, out CatoeditNum);
                    alimnew.Ca = CatoeditNum;

                    string Ptoedit = tbptoedit.Text;
                    decimal PtoeditNum;
                    decimal.TryParse(Ptoedit, out PtoeditNum);
                    alimnew.P = PtoeditNum;

                    string Prixtoedit = tbprixtoedit.Text;
                    decimal PrixtoeditNum;
                    decimal.TryParse(Prixtoedit, out PrixtoeditNum);
                    alimnew.PrixKg = PrixtoeditNum;


                    if (MStoeditNum == 0 || EtoeditNum == 0 || ProttoeditNum == 0 || MGtoeditNum == 0 || CatoeditNum == 0 || PtoeditNum == 0 || PrixtoeditNum == 0)
                    {
                        MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                    }

                    using (var context = new BDDDogalimEntities())
                    {

                        var alimtoedit = context.Feculents.Where(c => c.Id == cbalimtoedit.SelectedIndex + 2 - viandesDataGrid.Items.Count).First();

                        alimtoedit.MSeches = alimnew.MSeches;

                        alimtoedit.Energie = alimnew.Energie;

                        alimtoedit.Proteines = alimnew.Proteines;

                        alimtoedit.MGrasses = alimnew.MGrasses;

                        alimtoedit.Ca = alimnew.Ca;

                        alimtoedit.P = alimnew.P;

                        alimtoedit.PrixKg = alimnew.PrixKg;


                        context.SaveChanges();



                        LoadAlim();


                        cbalimtoedit.SelectedItem = null;

                    }
                }

                if (cbalimtoedit.SelectedIndex + 1 > viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count && cbalimtoedit.SelectedIndex + 1 <= viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count)
                {
                    Legumes alimnew = new Legumes();

                    string MStoedit = tbMStoedit.Text;
                    decimal MStoeditNum;
                    decimal.TryParse(MStoedit, out MStoeditNum);
                    alimnew.MSeches = MStoeditNum;

                    string Etoedit = tbEtoedit.Text;
                    decimal EtoeditNum;
                    decimal.TryParse(Etoedit, out EtoeditNum);
                    alimnew.Energie = EtoeditNum;

                    string Prottoedit = tbProttoedit.Text;
                    decimal ProttoeditNum;
                    decimal.TryParse(Prottoedit, out ProttoeditNum);
                    alimnew.Proteines = ProttoeditNum;

                    string MGtoedit = tbMGtoedit.Text;
                    decimal MGtoeditNum;
                    decimal.TryParse(MGtoedit, out MGtoeditNum);
                    alimnew.MGrasses = MGtoeditNum;

                    string Catoedit = tbcatoedit.Text;
                    decimal CatoeditNum;
                    decimal.TryParse(Catoedit, out CatoeditNum);
                    alimnew.Ca = CatoeditNum;

                    string Ptoedit = tbptoedit.Text;
                    decimal PtoeditNum;
                    decimal.TryParse(Ptoedit, out PtoeditNum);
                    alimnew.P = PtoeditNum;

                    string Prixtoedit = tbprixtoedit.Text;
                    decimal PrixtoeditNum;
                    decimal.TryParse(Prixtoedit, out PrixtoeditNum);
                    alimnew.PrixKg = PrixtoeditNum;


                    if (MStoeditNum == 0 || EtoeditNum == 0 || ProttoeditNum == 0 || MGtoeditNum == 0 || CatoeditNum == 0 || PtoeditNum == 0 || PrixtoeditNum == 0)
                    {
                        MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                    }

                    using (var context = new BDDDogalimEntities())
                    {

                        var alimtoedit = context.Legumes.Where(c => c.Id == cbalimtoedit.SelectedIndex + 3 - (viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count)).First();

                        alimtoedit.MSeches = alimnew.MSeches;

                        alimtoedit.Energie = alimnew.Energie;

                        alimtoedit.Proteines = alimnew.Proteines;

                        alimtoedit.MGrasses = alimnew.MGrasses;

                        alimtoedit.Ca = alimnew.Ca;

                        alimtoedit.P = alimnew.P;

                        alimtoedit.PrixKg = alimnew.PrixKg;


                        context.SaveChanges();



                        LoadAlim();


                        cbalimtoedit.SelectedItem = null;

                    }

                }

                if (cbalimtoedit.SelectedIndex + 1 > viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count && cbalimtoedit.SelectedIndex + 1 <= viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count + diversDataGrid.Items.Count)
                {

                    Divers alimnew = new Divers();

                    string MStoedit = tbMStoedit.Text;
                    decimal MStoeditNum;
                    decimal.TryParse(MStoedit, out MStoeditNum);
                    alimnew.MSeches = MStoeditNum;

                    string Etoedit = tbEtoedit.Text;
                    decimal EtoeditNum;
                    decimal.TryParse(Etoedit, out EtoeditNum);
                    alimnew.Energie = EtoeditNum;

                    string Prottoedit = tbProttoedit.Text;
                    decimal ProttoeditNum;
                    decimal.TryParse(Prottoedit, out ProttoeditNum);
                    alimnew.Proteines = ProttoeditNum;

                    string MGtoedit = tbMGtoedit.Text;
                    decimal MGtoeditNum;
                    decimal.TryParse(MGtoedit, out MGtoeditNum);
                    alimnew.MGrasses = MGtoeditNum;

                    string Catoedit = tbcatoedit.Text;
                    decimal CatoeditNum;
                    decimal.TryParse(Catoedit, out CatoeditNum);
                    alimnew.Ca = CatoeditNum;

                    string Ptoedit = tbptoedit.Text;
                    decimal PtoeditNum;
                    decimal.TryParse(Ptoedit, out PtoeditNum);
                    alimnew.P = PtoeditNum;

                    string Prixtoedit = tbprixtoedit.Text;
                    decimal PrixtoeditNum;
                    decimal.TryParse(Prixtoedit, out PrixtoeditNum);
                    alimnew.PrixKg = PrixtoeditNum;


                    if (MStoeditNum == 0 || EtoeditNum == 0 || ProttoeditNum == 0 || MGtoeditNum == 0 || CatoeditNum == 0 || PtoeditNum == 0 || PrixtoeditNum == 0)
                    {
                        MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                    }

                    using (var context = new BDDDogalimEntities())
                    {

                        var alimtoedit = context.Divers.Where(c => c.Id == cbalimtoedit.SelectedIndex + 4 - (viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count)).First();

                        alimtoedit.MSeches = alimnew.MSeches;

                        alimtoedit.Energie = alimnew.Energie;

                        alimtoedit.Proteines = alimnew.Proteines;

                        alimtoedit.MGrasses = alimnew.MGrasses;

                        alimtoedit.Ca = alimnew.Ca;

                        alimtoedit.P = alimnew.P;

                        alimtoedit.PrixKg = alimnew.PrixKg;


                        context.SaveChanges();



                        LoadAlim();


                        cbalimtoedit.SelectedItem = null;

                    }
                }

                if (cbalimtoedit.SelectedIndex + 1 > viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count + diversDataGrid.Items.Count)
                {
                    AlimIndus alimnew = new AlimIndus();

                    string MStoedit = tbMStoedit.Text;
                    decimal MStoeditNum;
                    decimal.TryParse(MStoedit, out MStoeditNum);
                    alimnew.MSeches = MStoeditNum;

                    string Etoedit = tbEtoedit.Text;
                    decimal EtoeditNum;
                    decimal.TryParse(Etoedit, out EtoeditNum);
                    alimnew.Energie = EtoeditNum;

                    string Prottoedit = tbProttoedit.Text;
                    decimal ProttoeditNum;
                    decimal.TryParse(Prottoedit, out ProttoeditNum);
                    alimnew.Proteines = ProttoeditNum;

                    string MGtoedit = tbMGtoedit.Text;
                    decimal MGtoeditNum;
                    decimal.TryParse(MGtoedit, out MGtoeditNum);
                    alimnew.MGrasses = MGtoeditNum;

                    string Catoedit = tbcatoedit.Text;
                    decimal CatoeditNum;
                    decimal.TryParse(Catoedit, out CatoeditNum);
                    alimnew.Ca = CatoeditNum;

                    string Ptoedit = tbptoedit.Text;
                    decimal PtoeditNum;
                    decimal.TryParse(Ptoedit, out PtoeditNum);
                    alimnew.P = PtoeditNum;

                    string Prixtoedit = tbprixtoedit.Text;
                    decimal PrixtoeditNum;
                    decimal.TryParse(Prixtoedit, out PrixtoeditNum);
                    alimnew.PrixKg = PrixtoeditNum;


                    if (MStoeditNum == 0 || EtoeditNum == 0 || ProttoeditNum == 0 || MGtoeditNum == 0 || CatoeditNum == 0 || PtoeditNum == 0 || PrixtoeditNum == 0)
                    {
                        MessageBox.Show("Au moins une des valeurs est nulle ou incorrecte \r\n\r\nVeuillez vous assurer que vos valeurs soient correctes (nombres décimaux, virgules \",\" uniquement, points \".\" interdits).", "Attention");
                    }

                    using (var context = new BDDDogalimEntities())
                    {

                        var alimtoedit = context.AlimIndus.Where(c => c.Id == cbalimtoedit.SelectedIndex + 5 - (viandesDataGrid.Items.Count + feculentsDataGrid.Items.Count + legumesDataGrid.Items.Count + diversDataGrid.Items.Count)).First();

                        alimtoedit.MSeches = alimnew.MSeches;

                        alimtoedit.Energie = alimnew.Energie;

                        alimtoedit.Proteines = alimnew.Proteines;

                        alimtoedit.MGrasses = alimnew.MGrasses;

                        alimtoedit.Ca = alimnew.Ca;

                        alimtoedit.P = alimnew.P;

                        alimtoedit.PrixKg = alimnew.PrixKg;


                        context.SaveChanges();



                        LoadAlim();


                        cbalimtoedit.SelectedItem = null;
                      
                    }
                }

                tbMStoedit.Text = string.Empty;
                tbEtoedit.Text = string.Empty;
                tbProttoedit.Text = string.Empty;
                tbMGtoedit.Text = string.Empty;
                tbcatoedit.Text = string.Empty;
                tbptoedit.Text = string.Empty;
                tbprixtoedit.Text = string.Empty;

            }
        }

       
    }
}
