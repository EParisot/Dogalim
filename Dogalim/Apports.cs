﻿using System.Collections.Generic;

namespace Dogalim
{
    public class Apports : List<double>
    {
        public decimal MS { get; set; }

        public decimal E { get; set; }

        public decimal Prot { get; set; }

        public decimal MG { get; set; }

        public decimal Ca { get; set; }

        public decimal P { get; set; }

        public decimal? Prix { get; set; }

    }
}
