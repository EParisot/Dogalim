﻿using System.IO;
using System.Xml.Serialization;

namespace Dogalim
{
    public class Dog
    {

        public string Name { get; set; }

        public int Sex { get; set; }

        public int Age { get; set; }

        public string Com { get; set; }


        public double PV { get; set; }

        public int cat { get; set; }
        public double Cat { get; set; }
        public int temp { get; set; }
        public double Temp { get; set; }
        public int etat { get; set; }
        public double Etat { get; set; }


        public int Reg { get; set; }


        public int Alim1 { get; set; }

        public int Alim2 { get; set; }

        public int Alim3 { get; set; }

        public int Alim4 { get; set; }


        public double precision { get; set; }


        /// <summary>
        /// Enregistre l'état courant de la classe dans un fichier au format XML.
        /// </summary>
        /// <param name="chemin"></param>
        /// 
        public void Enregistrer(string chemin)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Dog));
            StreamWriter r = new StreamWriter(chemin);
            xs.Serialize(r, this);
            r.Close();
        }

        public static Dog Charger(string chemin)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Dog));
            StreamReader r = new StreamReader(chemin);
            Dog dog = (Dog)xs.Deserialize(r);
            r.Close();

            return dog;
        }

    }
}
