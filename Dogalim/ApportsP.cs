﻿using System.Collections.Generic;

namespace Dogalim
{
    public class ApportsP : List<double>
    {
        public double Q { get; set; }

        public double MS { get; set; }

        public double E { get; set; }

        public double Prot { get; set; }

        public double MG { get; set; }

        public double Ca { get; set; }

        public double P { get; set; }
    }
}